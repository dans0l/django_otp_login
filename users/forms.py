from django import forms
from users.models import OTP, Client

# Form used in the OTP Page
# User will enter the OTP code 
# that will then be compared 
# with the OTP saved in the DB
class OTPForm(forms.ModelForm):
    otp_code = forms.CharField(max_length=6)
    
    class Meta:
        model = OTP
        fields = ['otp_code']

# Form used to Update User Detail/Profile Info
class ClientUpdateForm(forms.ModelForm):
    # this will make the fields below mandatory
    email = forms.EmailField()
    phone = forms.IntegerField()

    class Meta:
        model = Client
        fields = ['first_name', 'last_name', 'username', 'email', 'phone']