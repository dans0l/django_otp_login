import requests


def send(phone_number, message="Log In Code"):
    payload = {
        "username": 'sandbox',
        "to": f"+{phone_number}",
        "message": message,
        "from": 32524  # replace this with your short code
    }

    header = {
        "Accept": "application/json",
        "Content-Type": "application/x-www-form-urlencoded",
        # enter your api key here
        'apiKey': 'a876161d0cc72f5870269a8702ae8178ba39144649bd59715a79d8fd7869e80c'
    }

    req = requests.post('https://api.sandbox.africastalking.com/version1/messaging', data=payload, headers=header)

    req = req.json()
    message_data = req['SMSMessageData']
    status = message_data["Recipients"][0]['status']

    return status
