from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

# Create your models here.
class OTP(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    otp_code = models.CharField(null=True, max_length=6)
    date_created = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return f'{self.otp_code}'

# We will create this table and have it inherit the User Table
# but add the phone field that is absent in the User Table.

# This will create a One to One field with the User Table    
class Client(User):
    # mandatory field
    phone = models.IntegerField(null=True)

    @property
    def full_name(self):
        return f'{super().get_full_name()}'

    def __str__(self):
        return f'{super().get_full_name()}'