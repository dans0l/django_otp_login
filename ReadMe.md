# Implementation of One Time Passwords (O.T.P.) in Django

## Quick Guide
Below are the steps on how to get the web app up and running 

1.	Clone it:
```bash
    git clone https://bitbucket.org/dans0l/django_otp_login.git
```
2.	Cd into it:
```bash
    cd django_otp_login
```
3.	Create a venv
```bash
    python3 -m venv venv
```
4.	Activate venv:
```bash
    Mac/Linux: source venv/bin/activate
    Windows: venv\Scripts\activate
```
5.	Install the requirements
```bash
    pip install -r requirements.txt
```
6.	Create DB
```bash
    python manage.py makemigrations
```
7.	Apply DB Changes
```bash
    python manage.py migrate
```
8.	Configure your Africa's Talking SMS Shortcode and API Key:
    - Open the cloned project
    - Open the "users" folder
    - Open a python file labeled "send_message.py"
    - Enter the credentials in the file
9.	Run the server
```bash
    python manage.py runserver
```
10.	Navigate to your [localhost](http://127.0.0.1:8000) site
11.	Follow the instructions on the home page to start using the site
12.	Navigate to [Africa's Talking Simulator](https://developers.africastalking.com/simulator) to view Messages Sent
